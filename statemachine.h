/* statemachine.h:
 * 		This file is part of the Hex43 compiler project. It contains a
 * generic state machine generator.
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef STATEMACHINE_H_INC
#define STATEMACHINE_H_INC

#include <memory>
#include <string>

const std::string null_string = "";

/* Class state_machine:
 * 		Templated class to allow easy implementation of FSMs.
 * 
 * 		HOW TO USE:
 * 			- Make a type containing whatever data needs to be passed
 * 			  to the current state.
 * 			- If necessary, create a preprocessor function run before
 * 			  the actual state is invoked [preprocess()]
 * 			- Create a function for each state.  In addition, each
 * 			  state can be made a submachine by using substate_machine
 * 			- If necessary, specialize (INLINE and in the HEADER FILE)
 * 			  the finalize() method.
 * 			- The function names should pretty much be self explanitory.
 * 		
 * 		Hooray for function pointers.  The code was 5x longer and 10x
 * 		buggier before I implemented lexer as a state machine :D.
 * 
 * 	NOTE:  This class COPY CONSTRUCTS from the hints provided (at least
 * 		   for now), so *don't* try and use your old pointer- it's not the
 * 		   same object!  This was done to simplify this class's
 * 		   implementation.  At some point I should probably change it...
 * 
 * 	NOTE2:  If you're looking for a use of this class to reference, see the
 * 	        lexer.  It shows how to use everything, and this class was
 *          designed specifically with that implementation in mind.
 *  NOTE3:  These classes are mutual friends.  Yes, I know it's not 'best
 *          practice'.  But its the easiest way - and since these classes
 *          are only supposed to be extended upon through static polymorphism
 *          (template stuff, what functors get plugged in), it doesn't have
 *          much impact on anyone else.  You shouldn't *need* to extend upon
 *          them via inheritance.
 */

//forward declare to allow friendship
template <class Data, class T>
class substate_machine;

template <class Data, class T>
class state_machine {
	friend class substate_machine<Data, T>;
public:
	//public use typedefs
	typedef void (*state)(state_machine<Data, T>&, T);
	static void defstate(state_machine<Data, T>&, T);
	static void submachine_handle(state_machine<Data, T>&, T);
	//The above works with submachines because references are treated
	//by the standard like pointers- so polymorphism is allowed
private:
	//don't feel like writing a full on destructor for one pointer
	std::auto_ptr<Data> internal_data;
	state curstate;
	state returnstate;
	//this MUST be an auto_ptr or our memory management gets REAL tricky
	std::auto_ptr < state_machine<Data, T> > substate;
	void init();
	void call(state_machine<Data, T>&, T);
	//this method allows submachine to get data from top of hierarchy.
	virtual state_machine<Data, T> * parent(); //return TOP of tree
	void return_from_sub(); //this is a slot, to use the qt term
public:
	//public interface
	state_machine(Data * = NULL);
	Data& data();
	void change_state(state);
	void add_char(T);
	//NULL here means curstate:
	void start_submachine(state, state = NULL);
	virtual void state_return();
	//this method is available for specialization
	void finalize();
	void preprocess(T);
};

/* class substate_machine:
 * 		This class is a helper class to allow the creation of state
 * 		machines as states within another state machine.  Submachines:
 * 			-Share the same data.
 * 			-Behave exactly like a regular state, except upon exiting
 * 			 the submachine the state should call the state_return()
 * 			 method, which allows control to flow to the parent machine.
 * 			-Are invoked with the start_submachine() method.
 * 		Basically, what allows them to share data is the protected
 * 		virtual method parent(), which gets the state_machine object
 * 		at the hierarchy's root.  This is never used by the submachine,
 * 		only in the parent machine methods when accessing shared data
 * 		(i.e. the subclass provides 'plug-in' functionality with this
 * 		method), so it *could* be made a private virtual, but those seem
 * 		to be 1. poorly understood and 2. overprotective in cases like
 * 		this (i.e. do we *really* care if the submachine knows how to
 * 		access its parent? no, in fact, we encourage it).
 * 
 * 		The user should never see this class.  It is only to be used
 * 		by the state_machine parent class provide transparent operation
 * 		of substates (don't you love polymorphism>)
 */

template <class Data, class T>
class substate_machine : public state_machine<Data, T> {
	friend class state_machine<Data, T>;
private:
	state_machine<Data, T> * parentsm; //direct parent state machine
	substate_machine() {} //Default construction causes failure
	virtual state_machine<Data, T> * parent();
	substate_machine(state_machine<Data, T>*);
public:
	virtual void state_return(); //send a signal to the parent machine
};

// definitions

//note that state_machine<Data, T>::parent() returns the TOP of the
//hierarchy, NOT the direct parent.

template <class Data, class T>
state_machine<Data, T> * state_machine<Data, T>::parent() {
	return this; //base class state machine must be at top of hierarchy
}

template <class Data, class T>
void state_machine<Data, T>::finalize() {
	//this is left to be <intentionally> specialized over
}

template <class Data, class T>
void state_machine<Data, T>::preprocess(T preprocess_data) {
	//this is left to be <intentionally> specialized over
}

template <class Data, class T>
Data& state_machine<Data, T>::data() {
	//use parent here to allow all subs to access the hierarchy's shared
	//data as if they own it.
	return *(parent()->internal_data);
}

//these are two different functions for clarity's sake

template <class Data, class T>
void state_machine<Data, T>::defstate
(state_machine<Data, T>& self, T c) {
	//do nothing - default behavior
}

template <class Data, class T>
void state_machine<Data, T>::
submachine_handle(state_machine<Data, T>& self, T c) {
	//handle a submachine
	self.substate->curstate(*(self.substate), c);
}

template <class Data, class T>
void state_machine<Data, T>::state_return() {
	//should NOT happen, but handle just in case.
}

template <class Data, class T>
void state_machine<Data, T>::init() {
	curstate = defstate;
}

template <class Data, class T>
state_machine<Data, T>::state_machine
(Data * d) {
	init();
	//make a new data - copy construct if d is not null
	if (d) {
		internal_data = std::auto_ptr<Data>(new Data(*d));
	}
	else {
		internal_data = std::auto_ptr<Data>(new Data);
	}
}

template <class Data, class T>
void state_machine<Data, T>::change_state(state s) {
	curstate = s;
}

//the first state is the state to start a submachine in, the second
//state is the state to go into when the submachine returns to the
//parent, which is by default NULL (the current state)
template <class Data, class T>
void state_machine<Data, T>::start_submachine(state s, state rs) {
	//get arround default argument errors (static resolution...)
	if (rs == NULL) {
		rs = curstate;
	}
	//set up submachines
	substate = std::auto_ptr<state_machine<Data, T> >(new substate_machine<Data, T>(this));
	substate->change_state(s);
	returnstate = rs;
	//set up the submachine state handler
	curstate = submachine_handle;
}

//preprocess and then process a character through the state machine.
template <class Data, class T>
void state_machine<Data, T>::add_char(T c) {
	preprocess(c);
	curstate(*this, c);
}

//this is a slot for the submachine to send its return signal to.
//basically just switches the function pointer back.
template <class Data, class T>
void state_machine<Data, T>::return_from_sub() {
	curstate = returnstate;
}

//now for the substate

template <class Data, class T>
state_machine<Data, T> * substate_machine<Data, T>::parent() {
	//remember, this is the top of the hierarchy.
	return parentsm->parent();
}

template <class Data, class T>
substate_machine<Data, T>::
substate_machine(state_machine<Data, T> * sm) {
	this->init();
	parentsm = sm;
	//initialization.  MUST BE INITIALIZED BY A PARENT THROUGH THIS CTOR
}

template <class Data, class T>
void substate_machine<Data, T>::state_return() {
	parentsm->return_from_sub();
	//sends the parent the return signal.
}

#endif
