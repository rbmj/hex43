#Makefile for hex43

#using clang as its error messages are MUCH nicer
CC = clang++
CFLAGS = -g -Wall
OBJECTS = hex43c.o lex.o tokstruct.o
INCFLAGS = 
LDFLAGS = -Wl,-rpath,/usr/local/lib
LIBS = 

SUFFIXES = *.cpp *.h

all: hex43

hex43: $(OBJECTS)
	$(CC) -o hex43 $(OBJECTS) $(LDFLAGS) $(LIBS)

hex43c.o: hex43c.cpp hex43c.h tokstruct.h lex.h
	$(CC) -c -o hex43c.o hex43c.cpp $(CFLAGS) $(INCFLAGS) $(LIBS)
	
lex.o: lex.cpp lex.h statemachine.h hex43c.h
	$(CC) -c -o lex.o lex.cpp $(CFLAGS) $(INCFLAGS) $(LIBS)
	
tokstruct.o: tokstruct.cpp tokstruct.h lex.h hex43c.h
	$(CC) -c -o tokstruct.o tokstruct.cpp $(CFLAGS) $(INCFLAGS) $(LIBS)
	
parse.o: parse.cpp parse.h
	$(CC) -c -o parse.o parse.cpp $(CFLAGS) $(INCFLAGS) $(LIBS)

count:
	cat $(SUFFIXES) | wc -l

clean:
	rm -f *.o
