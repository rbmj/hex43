/* tokstruct.cpp:
 * 		This file is part of the Hex43 compiler project. It implements the
 * 'structurizer' defined in tokstruct.h
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tokstruct.h"
#include "hex43c.h"

void build_tree(statement& st, token_vector& src, unsigned& i, unsigned imax) {
	for (statement tmp; i < imax; i++) {
		token thistok = src.tokens.at(i);
		tmp.tokens.push_back(thistok);
		if (thistok->content == ";") {
			st.subblock.push_back(tmp);
			tmp.clear();
		}
		else if (thistok->content == "{") {
			build_tree(tmp, src, i, imax);
			st.subblock.push_back(tmp);
			tmp.clear();
		}
		else if (thistok->content == "}") {
			st.subblock.push_back(tmp);
			break;
		}
	}
}

const std::string double_ops = ":^&|<>+-="; //scope, logicals, shifts
const std::string cmpeq_ops = ":<>!";

//does setup, and is a special case of build_tree for global scope
file_statements structurize(token_vector& tok) {
	file_statements statements;
	statement tmp;
	statements.name = tok.file;
	for (unsigned i = 0; i < tok.tokens.size(); i++) {
		token thistok = tok.tokens.at(i);
		tmp.tokens.push_back(thistok);
		if (thistok->content == ";") {
			statements.global_scope.subblock.push_back(tmp);
			tmp.clear();
		}
		else if (thistok->content == "{") {
			build_tree(tmp, tok, ++i, tok.tokens.size());
			statements.global_scope.subblock.push_back(tmp);
			tmp.clear();
		}
		else if (thistok->content == "}") {
			error(statements.name, thistok->line, "Closing brace at global scope");
		}
	}
	if (!tmp.empty()) {
		statements.global_scope.subblock.push_back(tmp);
		error(statements.name, tok.tokens.back()->line, "Unterminated statement at EOF");
	}
	return statements;
}
