#ifndef PARSE_H_INC
#define PARSE_H_INC

#include <vector>
#include <string>
#include "lex.h"

//these classes are *not* very robust

//these classes form the table of accessible variables (so any varaible that
//is not local to a function).

enum access_specifier {
	PUBLIC,
	PRIVATE,
	PROTECTED
};

enum identifier_type {
	FUNCTION,
	VARIABLE,
	SCOPE
};

class identifier {
public:
	identifier_type itype;
	std::string name;
};

class function_identifier : public identifier {
public:
	std::string type;
	std::vector<variable_identifier*> args;
	~function_identifier();
};

class variable_identifier : public identifier {
public:
	std::string type;
};

enum scope_type {
	CLASS,
	NAMESPACE,
	STRUCT
};

class identifier_waccess : public identifier {
public:
	access_specifier access;
};
	

class scope_identifier : public identifier {
public:
	scope_type stype;
	std::vector<identifier_waccess*> sub;
	~scope_identifier();
};

class class_identifier : public scope_identifier {
public:
	//
};

class namespace_identifier : public scope_identifier {
public:
	//
};

class identifier_table {
public:
	std::vector<identifier*> ids;
	~identifier_table();
};

identifier_table build_idtable(file_statements&);

#endif