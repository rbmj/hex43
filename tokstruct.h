/* tokstruct.h:
 * 		This file is part of the Hex43 compiler project. It contains code to
 * give some structure to the code - separate into statements, blocks, scopes,
 * etc.  Kind of in-between the lexer and parser.
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TOKSTRUCT_H_INC
#define TOKSTRUCT_H_INC

#include "lex.h"
#include <vector>
#include <string>

class statement {
public:
	std::vector<statement> subblock;
	std::vector<token> tokens;
	void clear() {
		subblock.clear();
		tokens.clear();
	}
	bool empty() {
		return (subblock.empty() && tokens.empty());
	}
};

class file_statements {
public:
	std::string name;
	statement global_scope;
};

file_statements structurize(token_vector&);

#endif
