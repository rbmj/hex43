/* lex.cpp:
 * 		This file is part of the Hex43 compiler project. It contains the
 * lexer implementation in the state machine.  Each function (state) is
 * implemented under loose implicit 'contract' with the others.  Making
 * these contractsexplicit would be counter-productive, as this is much
 * easier to both read and implement.
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lex.h"
#include "hex43c.h"

token_vector lex(std::istream& stream, std::string fname) {
	//pass in some hints
	lex_data data;
	data.tokvec.file = fname;
	data.line = 1; //hard code cause it doesn't seem to work via ctor
	lex_machine lexer(&data);
	lexer.change_state(lex_main);
	for (char c = stream.get(); stream.good(); c = stream.get()) {
		lexer.add_char(c);
	}
	lexer.finalize(); //make sure everything is flushed
	return lexer.data().tokvec;
	//let's *hope* the compiler optimizes out the temporary (it won't).
}

//lex_data

//convenience function to push data into the token vector
void lex_data::pushcur() {
	if (!cur.empty()) {
		tokvec.tokens.push_back(new_token(cur, line));
		cur.clear();
	}
}

//a few lexer warnings:

void overflow_junk_warn(lex_machine& m) {
	warning(m.data().tokvec.file, m.data().line, garbage_stub);
}

void unterminated_bcomment_warn(lex_machine& m) {
	warning(m.data().tokvec.file, m.data().line, unterminated_stub);
}

//lex machine states:

//helper function that processes most characters.
void lex_helper(lex_data& data, char c) {
	//actual char processing
	if (whitespace.find(c) != std::string::npos) {
		data.pushcur();
	}
	else if (operators.find(c) != std::string::npos) {
		data.pushcur();
		data.cur.push_back(c);
		data.pushcur();
	}
	else if (std::isalnum(c)) {
		//add to the current token
		data.cur.push_back(c);
	}
} 

//determines what state the machine goes into, and how characters go to
//different states/functions.
void lex_main(lex_machine& mach, char c) {
	//highest level function - determines if mode should change
	if (c == '\'') { //single quote character- go to mode
		mach.data().pushcur();
		mach.data().cur = "'";
		mach.change_state(lex_squote);
	}
	else if (c == '"') {
		mach.data().pushcur();
		mach.data().cur = "\"";
		mach.start_submachine(dquote_main);
	}
	else if (c == '/') {
		mach.change_state(lex_fslash);
		//this is needed because of C-syle block comments
	}
	else {
		//no special substate, so fall through to helper char processor
		lex_helper(mach.data(), c);
	}
}

//handles single quotes.  Nothing too complicated
void lex_squote(lex_machine& mach, char c) {
	if (c == '\n') {
		mach.data().pushcur();
		mach.change_state(lex_main);
		//error will be detected by parser- no ending squote
	}
	else {
		mach.data().cur.push_back(c);
		if (c == '\'') {
			mach.data().pushcur();
			mach.change_state(lex_main);
		}
	}
}

//will NOT be in lex_fslash mode by end of execution. Char before c
//in source file will be a '/'
void lex_fslash(lex_machine& mach, char c) {
	if (c == '/') {
		mach.change_state(lex_lcomment);
	}
	else if (c == '*') {
		mach.start_submachine(bcomment_main, lex_main);
	}
	else {
		mach.change_state(lex_main);
		lex_helper(mach.data(), '/'); //the slash that was ignored last
		lex_helper(mach.data(), c);
	}
}

//c++ style line comment.  Again, pretty simple stuff.
void lex_lcomment(lex_machine& mach, char c) {
	if (c == '\n') {
		mach.change_state(lex_main);
	}
}

//double quote states:

//here's where it gets tricky.  Tricky enough to get its own machine.
void dquote_main(lex_machine& mach, char c) {
	if (c == '\\') {
		mach.change_state(dquote_bslash); //check if end-of-line escape
	}
	else if (c == '\n') {
		mach.data().pushcur();
		mach.state_return();
		//no closing quote added to token- parser will throw error
	}
	else {
		mach.data().cur.push_back(c);
		if (c == '"') {
			mach.data().pushcur();
			mach.state_return();
		}
	}
}

//check if its an end-of-line escape, and if so, handle it.  Otherwise,
//we don't care.
void dquote_bslash(lex_machine& mach, char c) {
	if (c == '\n') {
		mach.change_state(dquote_overflow);
	}
	else {
		//we know the last character is a bslash.
		mach.data().cur.push_back('\\');
		mach.data().cur.push_back(c);
		mach.change_state(dquote_main);
	}
}

/* Handle overflow.  This is done in a slightly more complicated fashion
 * than in most languages.  If the last character in a quote-line is a
 * backslash, then look on the next line for another quote character,
 * ignoring all preceeding characters.  This quotation mark signals the
 * start of the remainder of the quote.  EX:
 * 
 * 		"normal quote"
 * 		"a quote \
 * 		"with overflow"
 * 
 * This is useful in code, e.g.:
 * 		if (some_condition) {
 *			string x = "a quote that's way to long \
 * 					   "to fit on one line";
 * 		}
 * is legal.  Basically, the whole point is so you don't have to start
 * at the first character and kill your indentation, while still
 * remaining whitespace-neutral.
 */
void dquote_overflow(lex_machine& mach, char c) {
	if (c == '\n') {
		//error- bad quote overflow
		if (mach.data().flag) {
			overflow_junk_warn(mach);
			mach.data().flag = false;
		}
		mach.data().pushcur();
		mach.state_return();
	}
	else if (c == '"') {
		//end overflow mode
		if (mach.data().flag) {
			overflow_junk_warn(mach);
			mach.data().flag = false;
		}
		mach.change_state(dquote_main);
	}
	else if (whitespace.find(c) == std::string::npos) {
		//junk- emit warning
		mach.data().flag = true;
	}
}

//block comment states

/* simple.  We ignore.  However, this has to be its own statemachine for
 * a few reasons:
 * 	1. I want to allow nested block quotes, and
 *  2. The easiest way to do [1] is to be recursive, and starting a
 * 	   submachine of itself is this implementation of recursion.  It
 * 	   simplifys things considerably
 */
void bcomment_main(lex_machine& mach, char c) {
	if (c == '*') {
		//check to see if this is the end.
		mach.change_state(bcomment_star);
	}
	else if (c == '/') {
		//check to see if this is the start of a nested comment
		mach.change_state(bcomment_fslash);
	}
	else {
		//ignore
	}
}

void bcomment_star(lex_machine& mach, char c) {
	if (c == '/') {
		//end of comment.  To where do we return? lex_main()? another
		//bcomment submachine because this is actually a nested comment?
		//there's no way to tell.
		mach.state_return();
	}
	else if (c != '*') {
		//not the end of the comment. go back to main
		mach.change_state(bcomment_main);
	}
	else {
		//continue in this state - needed for **/
	}
}

void bcomment_fslash(lex_machine& mach, char c) {
	if (c == '*') {
		//start a submachine for a nested comment (recursion).  We want
		//to return to bcomment_main, not here, though, as *\* does not
		//end a comment *AND* set a new one. ^Use bslash above just in case
		mach.start_submachine(bcomment_main, bcomment_main);
	}
	else if (c != '/') {
		//not a new comment. go back to main
		mach.change_state(bcomment_main);
	}
	else {
		//continue in this state - needed for //*
	}
}
