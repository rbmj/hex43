/* hex43c.h:
 * 		This file is part of the Hex43 compiler project. It contains
 * project-wide definitions.
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HEX43C_H_INC
#define HEX43C_H_INC

//enable debug
#ifndef DEBUG
#define DEBUG
#endif

#include <string>
#include <vector>

void error(std::string, unsigned, std::string);
void warning(std::string, unsigned, std::string);

enum diagnostic_type {
	ERROR,
	WARNING
};

class diagnostic {
public:
	diagnostic_type type;
	std::string file;
	unsigned line;
	std::string message;
	
	//overload < operator for algorithm
	bool operator<(const diagnostic& other) const;
};
	

class diagnostic_table {
	static std::vector<diagnostic> msgs;
public:
	static void add(diagnostic d);
	static void sort();
	static void print(std::ostream& ostr);
	static void clear();
	static bool noerror();
};

#endif
