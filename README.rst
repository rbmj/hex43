==============================
The Hex43 Programming Language
==============================

This is a compiler project implementing the Hex43 Programming Language.
Currently it does absolutely nothing helpful (it's a **work in progress**).

Current Status:
---------------

The Lexer is working for the most part to tokenize the input.  The first pass
of the parser then takes this and sorts it into statements, blocks of code, etc.
Everything else has yet to be built.

Compiler Notes:
---------------

Currently the project only compiler with clang (or in this case clang++).  GCC
doesn't seem to like me very much, and since clang compiles the code (I've
also heard its implementation of the standard is better than GCCs), I haven't
taken the time to support gcc.

A Note:
-------

This compiler is horribly inefficient.  I know.  And I don't care.  The
purpose here is to build a functioning compiler to build a cool language
NOW, not twenty years from now.  If you want a *COOL* compiler, go to
the LLVM project (A great project, but I can't do *anything* on that
scale by myself).  Premature Optimization is the Root of All Evil.

Legal Crap:
-----------

All code in this project is Copyright |c| 2011,
Robert Blair Mason Jr. <rbmj@verizon.net> and is licensed
under the Apache License, version 2.0 (see LICENSE.rst).

.. |c| unicode:: 0xA9 .. (Copyright (c) Sign)
