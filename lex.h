/* lex.h:
 * 		This file is part of the Hex43 compiler project. It contains the
 * definitions for the lexer.
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LEX_H_INC
#define LEX_H_INC

#include "statemachine.h"
#include <vector>
#include <istream>
#include <string>
#include <boost/smart_ptr.hpp>

const std::string whitespace = " \t\n";
const std::string operators = "`~!@#$%^&*()-=+[]\\|}{;:,./?><";
//these are all the valid operators- note that not all are syntactically
//correct; some will cause a syntax error in the parser

const std::string bad_quote = "Malformed Quotation";
const std::string garbage_stub = "Garbage in string overflow, ignoring";
const std::string unterminated_stub = "Unterminated Block Comment at EOF, ignoring";

/* So the meat of the lexer:
 * 	Four classes are important here.  They are:
 * 		-token: contains a token and line information
 * 		-token_vector: set of tokens and file information, actual data that
 * 		 is returned by the lexer.
 * 		-lex_data:  Internal data used by the lexer itself.
 * 		-lex_machine:  This is an instantiation of the state_machine template
 * 		 described in 'state_machine.h', and does the work.  Contains a
 * 		 specialization of the finalize() function to finish off everything.
 * 
 * 	A few functions:
 * 		-lex() is the main lex function.  This is the ONLY function here that 
 * 		 is really *meant* to be a used outside of the lexer.
 * 		-lex_machine::finalize() is the last function called during lexing.
 * 		 It handles all cleanup, pushes partial tokens, etc.
 * 
 * 	All the other functions belong to the state machines:
 * 		+ files prefixed with lex_* are the main lex machine
 * 		+ files prefixed with dquote_* are part of the submachine which handles
 * 		  quotations (too complicated to be a simple state)
 * 		+ files prefixed with bcomment_* are part of the submachine which
 * 		  handles block comments (its own submachine to allow recursion)
 */

class token_t {
public:
	std::string content;
	unsigned line;
	token_t(std::string s, unsigned l) : content(s), line(l) {}
};

typedef boost::shared_ptr<token_t> token;

static inline token new_token(std::string s, unsigned l) {
	return token(new token_t(s, l));
}

class token_vector {
public:
	std::vector<token> tokens;
	std::string file;
	token_vector() : file(null_string) {}
	void clear() { tokens.clear(); file.clear(); }
};

/* this is used internally by the lexer - a token_vector is constructed
 * from the contents and returned by the main lex() function.
 */
class lex_data {
public:
	//data being processed:
	token_vector tokvec;
	//state:
	unsigned line;
	std::string cur;
	bool flag; //any machine that uses should clear on exit
	void pushcur(); //convenience function
	lex_data() : line(1), cur(null_string), flag(false) {}
};

//for our convenience: explicitly instantiate the lex_machine template
//and alias it so its less verbose and nicer
typedef state_machine<lex_data, char> lex_machine;
//that is, a state maching processing chars into lex_data

//main lex function
//first argument is an input stream, the second is the name of the stream.
token_vector lex(std::istream&, std::string);

//lexer warnings - not that many, the parser catches most stuff.
void overflow_junk_warn(lex_machine&);
void unterminated_bcomment_warn(lex_machine&);

//lex machine states
void lex_main(lex_machine&, char);
void lex_fslash(lex_machine&, char);
void lex_squote(lex_machine&, char);
void lex_lcomment(lex_machine&, char);

//double quote states
void dquote_main(lex_machine&, char);
void dquote_bslash(lex_machine&, char);
void dquote_overflow(lex_machine&, char);

//block comment states
void bcomment_main(lex_machine&, char);
void bcomment_star(lex_machine&, char);
void bcomment_fslash(lex_machine&, char);

//FOOTER: TEMPLATE SPECIALIZATION

//these HAVE to be inline or the compiler whines at you
template<> inline void lex_machine::finalize() {
	//push through any data still left in the token building buffer
	data().pushcur();
	//if it's in fslash mode, there still is a fslash that has not been
	//writen to the buffer yet, and thus the token vector.  Push it.
	if (curstate == lex_fslash) {
		data().cur = "/";
		data().pushcur();
	}
	//if there's a block comment at the end of the file, then warning
	else if (curstate == bcomment_main || curstate == bcomment_star ||
	    curstate == bcomment_fslash) {
		unterminated_bcomment_warn(*this);
	}
	//if we got interrupted in the middle of a dquote overflow run,
	//check to see if we need to issue a warning.
	else if (curstate == dquote_overflow && data().flag) {
		overflow_junk_warn(*this);
	}
}

//handles line increments transparantly so you (and EACH & EVERY state)
//don't have to.
template<> inline void lex_machine::preprocess(char c) {
	if (c=='\n') {
		data().line++;
	}
}

#endif
