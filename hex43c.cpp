/* hex43c.cpp:
 * 		This file is part of the Hex43 compiler project. It contains the
 * main implementation (and other stuff that doesn't fit anywhere).
 *  
 * Legal::
 * 
 * Copyright 2011 Robert Blair Mason Jr.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include "hex43c.h"
#include "lex.h"
#include "tokstruct.h"

bool diagnostic::operator<(const diagnostic& other) const {
	return (this->line < other.line);
}

void diagnostic_table::add(diagnostic d) {
	msgs.push_back(d);
}
void diagnostic_table::sort() {
	std::stable_sort(msgs.begin(), msgs.end());
}
void diagnostic_table::print(std::ostream& ostr) {
	for (unsigned i = 0; i < msgs.size(); i++) {
		diagnostic msg = msgs.at(i);
		if (msg.type == ERROR) {
			ostr << "Error[";
		}
		else if (msg.type == WARNING) {
			ostr << "Warning[";
		}
		else {
			ostr << "Message[";
		}
		ostr << msg.file << ", " << msg.line << "]: " << msg.message << std::endl;
	}
}
void diagnostic_table::clear() {
	msgs.clear();
}
bool diagnostic_table::noerror() {
	for (unsigned i = 0; i < msgs.size(); i++) {
		if (msgs.at(i).type == ERROR) {
			return false;
		}
	}
	return true;
}

std::vector<diagnostic> diagnostic_table::msgs;

void error(std::string file, unsigned line, std::string msg) {
   diagnostic d;
   d.type = ERROR;
   d.file = file;
   d.line = line;
   d.message = msg;
   diagnostic_table::add(d);
}

void warning(std::string file, unsigned line, std::string msg) {
   diagnostic d;
   d.type = WARNING;
   d.file = file;
   d.line = line;
   d.message = msg;
   diagnostic_table::add(d);
}


void debug_output_tokvec(token_vector& vec) {
	std::cout << "File " << vec.file << std::endl;
	for (unsigned i = 0; i < vec.tokens.size(); i++) {
		std::cout << "\t" << vec.tokens.at(i)->content
				  << " : " << vec.tokens.at(i)->line << std::endl;
		//output tokvec for debugging purposes
	}
}

void debug_output_filetoks(statement * st, unsigned level = 0) {
	for (unsigned i = 0; i < st->subblock.size(); i++) {
		for (unsigned j = 0; j < level; j++) {
			std::cout << "\t";
		}
		for (unsigned k = 0; k < st->subblock.at(i).tokens.size(); k++) {
			std::cout << st->subblock.at(i).tokens.at(k)->content << " ";
		}
		std::cout << std::endl;
		debug_output_filetoks(&st->subblock.at(i), level+1);
	}
}

int main() {
   std::string file;
   std::getline(std::cin, file);
   std::ifstream str;
   str.open(file.c_str());
   token_vector vec = lex(str, file);
   file_statements sts = structurize(vec);
   vec.clear();
   debug_output_filetoks(&sts.global_scope);
   diagnostic_table::sort();
   diagnostic_table::print(std::cerr);
   diagnostic_table::clear();
   std::cin.get();
   return 0;
}

   
